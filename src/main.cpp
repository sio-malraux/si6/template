/* This file is part of the 'template' program
 * (https://framagit.org/sio-malraux/si6/template)
 *
 * Copyright (C) 2020 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include <libpq-fe.h>

int main()
{
  return 0;
}

/*
  Ce fichier doit être compilé avec une laision vers la libpq, tel que
  la ligne de commande de compilation doit au moins contenir les
  informations suivantes :

  g++ $(pkg-config --cflags libpq) -o programme.executable main.cpp $(pkg-config --libs libpq)

*/
